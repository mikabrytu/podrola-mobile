package com.codex.mobile.android.podrola.action;

import android.content.Context;

import com.codex.mobile.android.podrola.model.Episode;
import com.codex.mobile.android.podrola.net.Endpoint;
import com.codex.mobile.android.podrola.net.Network;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by mikabrytu on 16/01/17.
 */

public class EpisodeAction {

    private Context context;
    private Network network;

    public EpisodeAction(Context context) {
        this.context = context;
        network = new Network(context);
    }

    public void get(String id, final OnEpisodeListener callback) {
        String url = Endpoint.get(Endpoint.GET_EPISODE_LIST) + id;
        network.request(url, false, new Network.NetworkListener() {
            @Override
            public void onResponse(Object response) {
                JsonArray array = new JsonParser().parse(response.toString()).getAsJsonObject().get("episodes").getAsJsonArray();

                List<Episode> list = new ArrayList<>();
                for (int i = 0; i < array.size(); i++) {
                    JsonObject object = array.get(i).getAsJsonObject();

                    Episode episode = new Episode();
                    episode.setId(object.get("id").getAsString());
                    episode.setTitle(object.get("title").getAsString());
                    episode.setLink(object.get("link").getAsString());
                    episode.setUrl(object.get("url").getAsString());
                    episode.setImage(object.get("image").getAsString());

                    list.add(episode);
                }

                callback.onResponse(list);
            }

            @Override
            public void onError(Object error) {
                callback.onError(error.toString());
            }

            @Override
            public Map<String, String> getParams() {
                return null;
            }
        });
    }

    public interface OnEpisodeListener {
        void onResponse(List<Episode> list);
        void onError(String message);
    }
}
