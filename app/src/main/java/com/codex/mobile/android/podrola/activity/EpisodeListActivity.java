package com.codex.mobile.android.podrola.activity;

import android.media.MediaPlayer;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;

import com.codex.mobile.android.podrola.R;
import com.codex.mobile.android.podrola.action.EpisodeAction;
import com.codex.mobile.android.podrola.adapter.EpisodeAdapter;
import com.codex.mobile.android.podrola.model.Episode;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class EpisodeListActivity extends AppCompatActivity {

    RecyclerView recycler;
    List<Episode> episodeList;
    SwipeRefreshLayout refresh;
    EpisodeAdapter adaper;

    private String podcastId;
    private MediaPlayer mediaPlayer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_episode_list);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(getIntent().getStringExtra("EXTRA_PODCAST_TITLE"));

        initFields();
        configFields();
        setListeners();

        prepareData();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void initFields() {
        recycler = (RecyclerView) findViewById(R.id.recycler_episode_list);
        refresh = (SwipeRefreshLayout) findViewById(R.id.content_episode_list);
        episodeList = new ArrayList<>();
        adaper = new EpisodeAdapter(EpisodeListActivity.this, episodeList);

        podcastId = getIntent().getStringExtra("EXTRA_PODCAST_ID");

    }

    private void configFields() {
        adaper.setActivity(this);

        RecyclerView.LayoutManager manager = new LinearLayoutManager(this);
        recycler.setLayoutManager(manager);
        recycler.setItemAnimator(new DefaultItemAnimator());
        recycler.setAdapter(adaper);
    }

    private void setListeners() {
        refresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                prepareData();
            }
        });
        refresh.post(new Runnable() {
            @Override
            public void run() {
                refresh.setRefreshing(true);
                prepareData();
            }
        });
    }

    private void prepareData() {
        EpisodeAction action = new EpisodeAction(EpisodeListActivity.this);
        action.get(podcastId, new EpisodeAction.OnEpisodeListener() {
            @Override
            public void onResponse(List<Episode> list) {
                episodeList = list;
                adaper.updateList(episodeList);
                refresh.setRefreshing(false);
            }

            @Override
            public void onError(String message) {

            }
        });
    }

    public void playPodcast(String url) throws IOException {
        killMediaPlayer();

        mediaPlayer = new MediaPlayer();
        mediaPlayer.setDataSource(url);
        mediaPlayer.prepare();
        mediaPlayer.start();
    }

    private void killMediaPlayer() {
        if (mediaPlayer != null) {
            try {
                mediaPlayer.stop();
                mediaPlayer.release();
                mediaPlayer = null;
            }
            catch(Exception e) {
                e.printStackTrace();
            }
        }
    }
}
