package com.codex.mobile.android.podrola.adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.codex.mobile.android.podrola.R;
import com.codex.mobile.android.podrola.activity.EpisodeListActivity;
import com.codex.mobile.android.podrola.model.Podcast;
import com.codex.mobile.android.podrola.util.RoundedImageView;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import java.util.List;

/**
 * Created by Mikabrytu on 14/12/2016.
 */

public class PodcastAdaper extends RecyclerView.Adapter<PodcastAdaper.ViewHolder> {

    private Context context;
    private List<Podcast> podcastList;

    public PodcastAdaper(Context context, List<Podcast> podcastList) {
        this.context = context;
        this.podcastList = podcastList;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.inflate_podcast_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        final Podcast podcast = podcastList.get(position);

        holder.title.setText(podcast.getTitle());
        holder.desc.setText(podcast.getDescription());
        holder.episodes.setText(String.valueOf(podcast.getEpisodes()));

        Target loadtarget = null;
        if (loadtarget == null) loadtarget = new Target() {
            @Override
            public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
                RoundedImageView rounded = new RoundedImageView(context);
                Bitmap image = rounded.getCroppedBitmap(bitmap, 150);

                holder.logo.setImageBitmap(image);
            }

            @Override
            public void onBitmapFailed(Drawable errorDrawable) {

            }

            @Override
            public void onPrepareLoad(Drawable placeHolderDrawable) {

            }
        };

        Picasso.with(context).load(podcast.getImage()).into(loadtarget);

        holder.layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, EpisodeListActivity.class);
                intent.putExtra("EXTRA_PODCAST_TITLE", podcast.getTitle());
                intent.putExtra("EXTRA_PODCAST_ID", podcast.getId());
                context.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return podcastList.size();
    }

    public Podcast getItem(int position) {
        return podcastList.get(position);
    }

    public void updateList(List<Podcast> newlist) {
        podcastList.clear();
        podcastList.addAll(newlist);
        this.notifyDataSetChanged();
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        RelativeLayout layout;
        ImageView logo;
        TextView title, desc, episodes;

        ViewHolder(View view) {
            super(view);

            layout = (RelativeLayout) view.findViewById(R.id.layout_podcasts_item);
            title = (TextView) view.findViewById(R.id.podcast_title);
            desc = (TextView) view.findViewById(R.id.podcast_desc);
            episodes = (TextView) view.findViewById(R.id.podcast_n_episodes);
            logo = (ImageView) view.findViewById(R.id.podcast_logo);
        }
    }
}
