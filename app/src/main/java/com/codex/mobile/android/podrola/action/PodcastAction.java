package com.codex.mobile.android.podrola.action;

import android.content.Context;
import android.util.Log;

import com.android.volley.VolleyError;
import com.codex.mobile.android.podrola.model.Podcast;
import com.codex.mobile.android.podrola.net.Endpoint;
import com.codex.mobile.android.podrola.net.Network;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by Mikabrytu on 14/12/2016.
 */

public class PodcastAction {

    private Context context;
    private Network network;

    public PodcastAction(Context context) {
        this.context = context;
        network = new Network(context);
    }

    public void getPodcast(final OnPodcastListener callback) {
        final List<Podcast> podcastList = new ArrayList<>();

        network.request(Endpoint.get(Endpoint.GET_PODCAST_LIST), false, new Network.NetworkListener() {
            @Override
            public void onResponse(Object response) {
                JsonArray array = new JsonParser().parse(String.valueOf(response)).getAsJsonArray();
                for (int i = 0; i < array.size(); i++) {
                    JsonObject object = new JsonParser().parse(String.valueOf(array.get(i))).getAsJsonObject();
                    String episodes = String.valueOf(object.get("episodes"));

                    Podcast podcast = new Podcast();
                    podcast.setId(object.get("id").getAsString());
                    podcast.setTitle(object.get("title").getAsString());
                    podcast.setLink(object.get("link").getAsString());
                    podcast.setFeed(object.get("feed").getAsString());
                    podcast.setDescription(object.get("description").getAsString());
                    podcast.setAuthor(object.get("author").getAsString());
                    podcast.setImage(object.get("image").getAsString());
                    podcast.setEpisodes(Integer.parseInt(episodes));

                    podcastList.add(podcast);
                }
                Log.d("TAG", "response");

                callback.onResponse(podcastList);
            }

            @Override
            public void onError(Object error) {
                VolleyError e = (VolleyError) error;
                Log.e("Error Response", e.getMessage());;

                callback.onError(e.getMessage());
            }

            @Override
            public Map<String, String> getParams() {
                return null;
            }
        });
    }

    public interface OnPodcastListener {
        void onResponse(List<Podcast> list);
        void onError(String message);
    }
}
