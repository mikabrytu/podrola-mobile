package com.codex.mobile.android.podrola.adapter;

import android.content.Context;
import android.media.MediaPlayer;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.codex.mobile.android.podrola.R;
import com.codex.mobile.android.podrola.activity.EpisodeListActivity;
import com.codex.mobile.android.podrola.model.Episode;

import java.io.IOException;
import java.util.List;

/**
 * Created by mikabrytu on 16/01/17.
 */

public class EpisodeAdapter extends RecyclerView.Adapter<EpisodeAdapter.ViewHolder> {

    private Context context;
    private List<Episode> list;
    private EpisodeListActivity episodeListActivity;

    public EpisodeAdapter(Context context, List<Episode> list) {
        this.context = context;
        this.list = list;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.inflate_episode_list_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        final Episode episode = list.get(position);

        holder.title.setText(episode.getTitle());
        holder.layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //TODO: Trocar ícone de play
                holder.icon.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_pause_grey_800_48dp));
                //TODO: Tocar episódio por stream
                try {
                    episodeListActivity.playPodcast(episode.getUrl());
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public void updateList(List<Episode> list) {
        this.list.clear();
        this.list.addAll(list);
        this.notifyDataSetChanged();
    }

    public void setActivity(EpisodeListActivity activity) {
        episodeListActivity = activity;
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        ImageView icon;
        TextView title;
        LinearLayout layout;

        ViewHolder(View view) {
            super(view);

            title = (TextView) view.findViewById(R.id.title_episode_list);
            icon = (ImageView) view.findViewById(R.id.icon_episode_play);
            layout = (LinearLayout) view.findViewById(R.id.layout_episode_item);
        }
    }

}
