package com.codex.mobile.android.podrola.net;

import android.content.Context;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import java.util.Map;
import java.util.concurrent.TimeUnit;

/**
 * Created by Mikabrytu on 14/12/2016.
 */

public class Network {

    private Context context;
    private RequestQueue requestQueue;

    public Network(Context context) {
        this.context = context;
        requestQueue = Volley.newRequestQueue(context);
    }

    public RequestQueue getRequestQueue() {
        return requestQueue;
    }

    public void request(String url, boolean isPost, final NetworkListener callback) {
        int method = (isPost) ? Request.Method.POST : Request.Method.GET;

        StringRequest request = new StringRequest(method, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                callback.onResponse(response);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                callback.onError(error);
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                return callback.getParams();
            }
        };

        request.setRetryPolicy(new DefaultRetryPolicy((int) TimeUnit.SECONDS.toMillis(5),0,DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(request);
    }

    public interface NetworkListener {
        void onResponse(Object response);
        void onError(Object error);
        Map<String, String> getParams();
    }
}
