package com.codex.mobile.android.podrola.net;

/**
 * Created by Mikabrytu on 14/12/2016.
 */

public class Endpoint {

    private static String HOST = "http://podcast.lucandrade.com.br/";

    public static String GET_PODCAST_LIST = "api/podcasts";

    //api/podcasts/{id}
    public static String GET_EPISODE_LIST = "api/podcasts/";

    public static String get(String endpoint) {
        return HOST + endpoint;
    }
}
