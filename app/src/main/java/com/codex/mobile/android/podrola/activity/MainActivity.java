package com.codex.mobile.android.podrola.activity;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.codex.mobile.android.podrola.R;
import com.codex.mobile.android.podrola.action.PodcastAction;
import com.codex.mobile.android.podrola.adapter.PodcastAdaper;
import com.codex.mobile.android.podrola.model.Podcast;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    List<Podcast> podcastList;
    SwipeRefreshLayout refresh;
    RecyclerView recyclerView;
    PodcastAdaper adaper;
    FloatingActionButton fab;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        initFields();
        configFields();
        setListeners();

        prepareData();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        switch (id) {
            case R.id.action_settings:
                break;

            case R.id.action_search:
                Toast.makeText(MainActivity.this, "Searching...", Toast.LENGTH_SHORT).show();
                break;
            default:
                break;
        }

        return true;
    }

    private void initFields() {
        fab = (FloatingActionButton) findViewById(R.id.fab);

        podcastList = new ArrayList<>();

        recyclerView = (RecyclerView) findViewById(R.id.recycler_podcast_list);
        refresh = (SwipeRefreshLayout) findViewById(R.id.content_main);
        adaper = new PodcastAdaper(this, podcastList);
    }

    private void configFields() {
        RecyclerView.LayoutManager manager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(manager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(adaper);
    }

    private void setListeners() {
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(MainActivity.this, "Em breve", Toast.LENGTH_SHORT).show();
            }
        });

        refresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                prepareData();
            }
        });
        refresh.post(new Runnable() {
            @Override
            public void run() {
                refresh.setRefreshing(true);
                prepareData();
            }
        });
    }

    private void prepareData() {
        PodcastAction action = new PodcastAction(this);
        action.getPodcast(new PodcastAction.OnPodcastListener() {
            @Override
            public void onResponse(List<Podcast> list) {
                podcastList = list;
                adaper.updateList(podcastList);
                refresh.setRefreshing(false);
            }

            @Override
            public void onError(String message) {
                Toast.makeText(MainActivity.this, message, Toast.LENGTH_SHORT).show();
            }
        });
    }
}